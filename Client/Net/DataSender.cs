﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

public enum ClientPackets
{
    CHelloServer = 1,
}

static class DataSender
{
    public static void SendHelloServer()
    {
        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteInt((int)ClientPackets.CHelloServer);
        buffer.WriteString("Hi server!");
        ClientTCP.SendData(buffer.ToArray());
        buffer.Dispose();
    }
}
