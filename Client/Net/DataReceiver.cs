﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public enum ServerPackets
{
    SWelcomeMessage = 1,
}

static class DataReceiver
{
    public static void HandleWelcomeMessage(byte[] data)
    {
        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteByteArray(data);
        int packetID = buffer.ReadInt();
        string msg = buffer.ReadString();
        buffer.Dispose();
        
        Debug.Log(msg);
        DataSender.SendHelloServer();
    }
}
