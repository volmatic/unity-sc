﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

static class PacketHandle
{
    private static ByteBuffer playerBuffer;
    public delegate void Packet(byte[] data);
    public static Dictionary<int, Packet> Packets = new Dictionary<int, Packet>();

    public static void InitializePackets()
    {
        Packets.Add((int)ServerPackets.SWelcomeMessage, DataReceiver.HandleWelcomeMessage);
    }

    public static void HandleData(byte[] data)
    {
        byte[] buffer = (byte[])data.Clone();
        int packetLength = 0;

        if (playerBuffer == null)
            playerBuffer = new ByteBuffer();

        playerBuffer.WriteByteArray(buffer);

        if (playerBuffer.Count() == 0)
        {
            playerBuffer.Clear();
            return;
        }

        if (playerBuffer.Length() >= 4)
        {
            packetLength = playerBuffer.ReadInt(false);
            if (packetLength <= 0)
            {
                playerBuffer.Clear();
                return;
            }
        }

        while (packetLength > 0 && packetLength <= playerBuffer.Length() - 4)
        {
            if (packetLength <= playerBuffer.Length() - 4)
            {
                playerBuffer.ReadInt();
                data = playerBuffer.ReadByteArray(packetLength);
                HandleDataPackets(data);
            }

            packetLength = 0;
            if (playerBuffer.Length() >= 4)
            {
                packetLength = playerBuffer.ReadInt(false);
                if (packetLength <= 0)
                {
                    playerBuffer.Clear();
                    return;
                }
            }
        }

        if (packetLength <= 1)
        {
            playerBuffer.Clear();
        }
    }

    private static void HandleDataPackets(byte[] data)
    {
        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteByteArray(data);
        int packetID = buffer.ReadInt();
        buffer.Dispose();

        if (Packets.TryGetValue(packetID, out Packet packet))
        {
            packet.Invoke(data);
        }
    }
}
