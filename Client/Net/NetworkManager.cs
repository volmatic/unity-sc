﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkManager : MonoBehaviour
{
    private static NetworkManager instance;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        DontDestroyOnLoad(this);
        UnityThread.initUnityThread();

        PacketHandle.InitializePackets();
        ClientTCP.InitializeNetwork();
    }

    private void OnApplicationQuit()
    {
        ClientTCP.Disconnect();
    }
}
