﻿using System;
using System.Collections.Generic;
using System.Text;

public class ByteBuffer : IDisposable
{
    private List<byte> Buff;
    private byte[] readBuff;
    private int readPos;
    private bool buffUpdated = false;

    public ByteBuffer()
    {
        Buff = new List<byte>();
        readPos = 0;
    }

    public int GetReadPos()
    {
        return readPos;
    }

    public byte[] ToArray()
    {
        return Buff.ToArray();
    }

    public int Count()
    {
        return Buff.Count;
    }

    public int Length()
    {
        return Count() - readPos;
    }

    public void Clear()
    {
        Buff.Clear();
        readPos = 0;
    }

    #region Write
    public void WriteByte(byte input)
    {
        Buff.Add(input);
        buffUpdated = true;
    }

    public void WriteByteArray(byte[] input)
    {
        Buff.AddRange(input);
        buffUpdated = true;
    }

    public void WriteShort(short input)
    {
        Buff.AddRange(BitConverter.GetBytes(input));
        buffUpdated = true;
    }

    public void WriteInt(int input)
    {
        Buff.AddRange(BitConverter.GetBytes(input));
        buffUpdated = true;
    }

    public void WriteLong(long input)
    {
        Buff.AddRange(BitConverter.GetBytes(input));
        buffUpdated = true;
    }

    public void WriteFloat(float input)
    {
        Buff.AddRange(BitConverter.GetBytes(input));
        buffUpdated = true;
    }

    public void WriteBool(bool input)
    {
        Buff.AddRange(BitConverter.GetBytes(input));
        buffUpdated = true;
    }

    public void WriteString(string input)
    {
        Buff.AddRange(BitConverter.GetBytes(input.Length));
        Buff.AddRange(Encoding.ASCII.GetBytes(input));
        buffUpdated = true;
    }
    #endregion

    #region Read
    public byte ReadByte(bool Peek = true)
    {
        if (Buff.Count > readPos)
        {
            if (buffUpdated)
            {
                readBuff = Buff.ToArray();
                buffUpdated = false;
            }

            byte value = readBuff[readPos];
            if (Peek && Buff.Count > readPos)
            {
                readPos += 1;
            }

            return value;
        }
        else
        {
            throw new Exception("Trying to read something other than a byte in ReadByte");
        }
    }

    public byte[] ReadByteArray(int Length, bool Peek = true)
    {
        if (Buff.Count > readPos)
        {
            if (buffUpdated)
            {
                readBuff = Buff.ToArray();
                buffUpdated = false;
            }

            byte[] value = Buff.GetRange(readPos, Length).ToArray();
            if (Peek)
            {
                readPos += Length;
            }

            return value;
        }
        else
        {
            throw new Exception("Trying to read something other than a byte array in ReadByteArray");
        }
    }

    public short ReadShort(bool Peek = true)
    {
        if (Buff.Count > readPos)
        {
            if (buffUpdated)
            {
                readBuff = Buff.ToArray();
                buffUpdated = false;
            }

            short value = BitConverter.ToInt16(readBuff, readPos);
            if (Peek && Buff.Count > readPos)
            {
                readPos += 2;
            }

            return value;
        }
        else
        {
            throw new Exception("Trying to read something other than a short in ReadShort");
        }
    }

    public int ReadInt(bool Peek = true)
    {
        if (Buff.Count > readPos)
        {
            if (buffUpdated)
            {
                readBuff = Buff.ToArray();
                buffUpdated = false;
            }

            int value = BitConverter.ToInt32(readBuff, readPos);
            if (Peek && Buff.Count > readPos)
            {
                readPos += 4;
            }

            return value;
        }
        else
        {
            throw new Exception("Trying to read something other than a int in ReadInt");
        }
    }

    public long ReadLong(bool Peek = true)
    {
        if (Buff.Count > readPos)
        {
            if (buffUpdated)
            {
                readBuff = Buff.ToArray();
                buffUpdated = false;
            }

            long value = BitConverter.ToInt64(readBuff, readPos);
            if (Peek && Buff.Count > readPos)
            {
                readPos += 8;
            }

            return value;
        }
        else
        {
            throw new Exception("Trying to read something other than a long in ReadLong");
        }
    }

    public float ReadFloat(bool Peek = true)
    {
        if (Buff.Count > readPos)
        {
            if (buffUpdated)
            {
                readBuff = Buff.ToArray();
                buffUpdated = false;
            }

            float value = BitConverter.ToSingle(readBuff, readPos);
            if (Peek && Buff.Count > readPos)
            {
                readPos += 4;
            }

            return value;
        }
        else
        {
            throw new Exception("Trying to read something other than a float in ReadFloat");
        }
    }

    public bool ReadBool(bool Peek = true)
    {
        if (Buff.Count > readPos)
        {
            if (buffUpdated)
            {
                readBuff = Buff.ToArray();
                buffUpdated = false;
            }

            bool value = BitConverter.ToBoolean(readBuff, readPos);
            if (Peek && Buff.Count > readPos)
            {
                readPos += 1;
            }

            return value;
        }
        else
        {
            throw new Exception("Trying to read something other than a bool in ReadBool");
        }
    }

    public string ReadString(bool Peek = true)
    {
        try
        {
            int length = ReadInt(true);
            if (buffUpdated)
            {
                readBuff = Buff.ToArray();
                buffUpdated = false;
            }

            string value = Encoding.ASCII.GetString(readBuff, readPos, length);
            if (Peek && Buff.Count > readPos)
            {
                if (value.Length > 0)
                    readPos += length;
            }

            return value;
        }
        catch
        {
            throw new Exception("Trying to read something other than a string in ReadString");
        }
    }
    #endregion

    private bool disposedValue = false;
    protected virtual void Dispose(bool disposing)
    {
        if (!disposing)
        {
            if (disposing)
            {
                Buff.Clear();
                readPos = 0;
            }
            disposedValue = true;
        }
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }
}