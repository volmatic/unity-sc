﻿using System;
using System.Net;
using System.Net.Sockets;

static class ClientTCP
{
    private static TcpClient clientSocket;
    private static NetworkStream myStream;
    private static byte[] recBuffer;

    public static void InitializeNetwork()
    {
        clientSocket = new TcpClient();
        clientSocket.ReceiveBufferSize = 4096;
        clientSocket.SendBufferSize = 4096;
        recBuffer = new byte[4096 * 2];
        clientSocket.BeginConnect("127.0.0.1", 82, new AsyncCallback(ClientConnectCallback), null);
    }

    private static void ClientConnectCallback(IAsyncResult ar)
    {
        clientSocket.EndConnect(ar);
        if (clientSocket.Connected == false)
            return;

        clientSocket.NoDelay = true;
        myStream = clientSocket.GetStream();
        myStream.BeginRead(recBuffer, 0, 4096 * 2, ReceiveCallback, null);
    }

    private static void ReceiveCallback(IAsyncResult ar)
    {
        try
        {
            int length = myStream.EndRead(ar);
            if (length <= 0)
            {
                return;
            }

            byte[] newBytes = new byte[length];
            Array.Copy(recBuffer, newBytes, length);

            UnityThread.executeInFixedUpdate(() =>
            {
                PacketHandle.HandleData(newBytes);
            });
        }
        catch (Exception)
        {
            return;
        }
    }

    public static void SendData(byte[] data)
    {
        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteInt((data.GetUpperBound(0) - data.GetLowerBound(0)) + 1);
        buffer.WriteByteArray(data);
        myStream.BeginWrite(buffer.ToArray(), 0, buffer.ToArray().Length, null, null);
        buffer.Dispose();
    }

    public static void Disconnect()
    {
        clientSocket.Close();
    }
}
